# Cluster reset

```
systemctl stop pveproxy.service pve-cluster.service pve-ha-crm.service pve-ha-lrm.service corosync.service && \
mv /etc/corosync/corosync.conf{,.$(date +%s).bak} && \
mv /etc/corosync/authkey{,.$(date +%s).bak} && \
pmxcfs -l && \
rm /etc/pve/corosync.conf && \
pkill pmxcfs && \
systemctl restart pvedaemon.service && \
systemctl start pveproxy.service pve-ha-crm.service pve-ha-lrm.service
```