# ssh-keygen
ssh-keygen -R "${ssh_hostname?}"

# sed
`sed -i "${line_number?}d" ~/.ssh/known_hosts`